package com.example.InventoryManagementSystem.Security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class ApplicationConfiguration extends WebSecurityConfigurerAdapter {

        @Bean
        public PasswordEncoder passwordEncoder(){
            return new BCryptPasswordEncoder();
        }

        @Autowired
        private UserDetailsService userDetailsService;

        AuthenticationProvider authenticationProvider(){
            DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
            authenticationProvider.setUserDetailsService(userDetailsService);
            authenticationProvider.setPasswordEncoder(passwordEncoder());
            return authenticationProvider;
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.csrf().disable();
            http.cors().disable();
            http.authorizeRequests()
                    .antMatchers("/").permitAll()
                    .antMatchers("/api/users/{userId}").hasAuthority("ADMIN")
                    .antMatchers("/api/users/listOfUsers").hasAuthority("ADMIN")
                    .antMatchers("/api/products/addProduct").hasAuthority("ADMIN")
                    .antMatchers("/api/products/update{ProductId}").hasAuthority("MANAGER")
                    .antMatchers("/api/products/delete{ProductId}").hasAuthority("ADMIN")
                    .antMatchers("/api/products/get/{ProductId}").hasAuthority("CUSTOMER")
                    .antMatchers("/api/products/allProducts").hasAuthority("ADMIN")
                    .antMatchers("/api/orderDetails/get/{id}").hasAuthority("ADMIN")

                    .antMatchers("/api/orderDetails/{id}").hasAuthority("ADMIN")
                    .antMatchers("/api/inventories/allInventories").hasAuthority("ADMIN")
                    .antMatchers("/api/inventories/addInventory").hasAuthority("ADMIN")
                    .antMatchers("/api/inventories/{productId}").hasAuthority("ADMIN")
                    .antMatchers("/api/inventories/delete/{inventoryId}").hasAuthority("ADMIN")
                    .anyRequest().authenticated().and().httpBasic();
        }
    }



