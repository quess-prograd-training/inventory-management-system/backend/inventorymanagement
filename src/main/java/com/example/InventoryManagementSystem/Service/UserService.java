package com.example.InventoryManagementSystem.Service;

import com.example.InventoryManagementSystem.Entity.Role;
import com.example.InventoryManagementSystem.Entity.User;
import com.example.InventoryManagementSystem.Repository.RoleRepository;
import com.example.InventoryManagementSystem.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserService implements UserServiceInterface {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    public UserService(UserRepository userRepository){
        this.userRepository=userRepository;
    }
    @Override
    public User addUser(User newUser) {
        newUser.setPassword(this.passwordEncoder.encode(newUser.getPassword()));

        List<User> user=userRepository.findAll();
        if (!user.isEmpty()){
            Role roles=roleRepository.findById(2L).orElseThrow();
            Set<Role> roleObj=new HashSet<>();
            roleObj.add(roles);
            newUser.setRoles(roleObj);
            return userRepository.save(newUser);
        }
        else {//if customer not available then make role as admin
            Role roleObj1=new Role();
            roleObj1.setRoleName("ADMIN");
            roleRepository.save(roleObj1);

            Role roleObj2=new Role();
            roleObj2.setRoleName("CUSTOMER");
            roleRepository.save(roleObj2);

            Role roles=roleRepository.findById(1l).orElseThrow();//by default first customer role is admin
            Set<Role> roleObj=new HashSet<>();
            roleObj.add(roles);
            newUser.setRoles(roleObj);
            return userRepository.save(newUser);
        }
    }
    @Override
    public String deleteUserById(int userId) {
        User existingCustomer=userRepository.findById(userId).get();
        existingCustomer.getRoles().clear();//first clear role before delete due to foreign key constraint
        userRepository.delete(userRepository.findById(userId).get());
        return "Deleted customer id: "+userId;
    }
    @Override
    public User updateUserById(User user, int userId) {
        User existingUser=userRepository.findById(userId).get();
        existingUser.setEmail(user.getEmail());
        existingUser.setUserName(user.getUsername());
        existingUser.setPhoneNumber(user.getPhoneNumber());
        existingUser.setPassword(this.passwordEncoder.encode(user.getPassword()));
        userRepository.save(existingUser);
        return userRepository.findById(userId).get();
    }
    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
    @Override
    public User getUserById(int userId) {
        return userRepository.findById(userId).get();
    }
}
