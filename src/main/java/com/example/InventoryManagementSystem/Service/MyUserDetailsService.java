package com.example.InventoryManagementSystem.Service;


import com.example.InventoryManagementSystem.Entity.User;
import com.example.InventoryManagementSystem.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepositoryObject;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user = userRepositoryObject.findByUserName(userName);
        if (user == null) {
            throw new UsernameNotFoundException("User not found!..");
        }
        return user;
    }
}


