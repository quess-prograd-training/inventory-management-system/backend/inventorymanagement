package com.example.InventoryManagementSystem.Service;

import com.example.InventoryManagementSystem.Entity.Inventory;
import com.example.InventoryManagementSystem.Entity.Product;
import com.example.InventoryManagementSystem.Repository.InventoryRepository;
import com.example.InventoryManagementSystem.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class InventoryService implements InventoryServiceInterface {
    @Autowired
    InventoryRepository inventoryRepositoryObject;

    @Autowired
    ProductService productServiceObject;
    @Autowired
    ProductRepository productRepository;
    @Override
    public Boolean isStockAvailable(Integer quantity, int productId){
       // if(getStockInProductById(productId) >=quantity){
        Optional<Product> productOptional = productRepository.findById(productId);
        if(productOptional.isPresent()) {
            Product product = productOptional.get();
            Inventory inventory = product.getInventories();
            if (inventory != null && inventory.getStockIn() >= quantity) {
                // Inventory existObject = productRepository.findById(productId).get().getInventories();
                inventory.setStockIn(inventory.getStockIn() - quantity);//decrement the stockIn
                if(inventory.getStockOut()==null) inventory.setStockOut(quantity);//adding stockOut
                else inventory.setStockOut(inventory.getStockOut() + quantity);
                inventoryRepositoryObject.save(inventory);
                return true;
            }
            return false;
        }
        else {
            throw new NoSuchElementException("No product found with id " + productId);
        }
    }

    @Override
    public String getAllProductStockById(int productId) {
        Product product=productRepository.findById(productId).get();
        Inventory inventory=product.getInventories();
        return "Product: "+product.getProductName()+" stock left: "+inventory.getStockIn();
    }



    @Override
    public Integer getStockInProductById(int productId) {
        Optional<Product> productOptional=productRepository.findById(productId);
        if(productOptional.isPresent()) {
            Product product = productOptional.get();
            Inventory inventory = product.getInventories();
            return inventory.getStockIn();
        }
        else{
            throw new NoSuchElementException("No product found with id " + productId);
        }
    }

    @Override
    public String getAllProductStock(){
        StringBuilder string = new StringBuilder();
        List<Product> allProducts = productServiceObject.getAllProducts();
        string.append("All products Stock left\n ");
        for(Product product:allProducts) {
            if (product.getInventories() != null) {
                string.append(product.getProductName() + ":" + product.getInventories().getStockIn() + "\n");
            } else {
                string.append(product.getProductName() + ":Not Available\n");
            }
        }
        return string.toString();

    }

    @Override
    public Product addStockByProductId(int productId, Inventory inventoryObject) {
        Product existProduct = productRepository.findById(productId).orElseThrow();
        if (existProduct.getInventories() != null) {
            Inventory existInventory = inventoryRepositoryObject.findById(existProduct.getInventories().getInventoryId()).get();
            existInventory.setStockIn(inventoryObject.getStockIn() + existInventory.getStockIn());
            inventoryRepositoryObject.save(existInventory);
            existProduct = productRepository.findById(productId).orElseThrow();
        }
        else {
            throw new IllegalArgumentException("Inventories not found for the product");
        }
        return existProduct;

        }
}

