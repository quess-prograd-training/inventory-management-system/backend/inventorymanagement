package com.example.InventoryManagementSystem.Service;

import com.example.InventoryManagementSystem.Entity.User;

import java.util.List;

public interface UserServiceInterface {

    User addUser(User newUser);

    String deleteUserById(int userId);

    User updateUserById(User user, int userId);

    List<User> getAllUsers();

    User getUserById(int userId);
}
