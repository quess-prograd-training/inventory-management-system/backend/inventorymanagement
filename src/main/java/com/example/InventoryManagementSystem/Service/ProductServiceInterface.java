package com.example.InventoryManagementSystem.Service;

import com.example.InventoryManagementSystem.Entity.Product;

import java.util.List;

public interface ProductServiceInterface {
    List<Product> getAllProducts();

    Product getProductById(int productId);

    Product createProduct(Product productObject);

    Product updateProduct(int productId, Product productObject);

    String deleteProduct(int productId);
}
