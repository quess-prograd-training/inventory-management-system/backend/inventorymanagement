package com.example.InventoryManagementSystem.Service;

import com.example.InventoryManagementSystem.Entity.Product;
import com.example.InventoryManagementSystem.Repository.ProductRepository;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ProductService implements ProductServiceInterface {
    @Autowired
    ProductRepository productRepositoryObject;

    @Override
    public List<Product> getAllProducts() {
       return productRepositoryObject.findAll();
    }
    @Override
    public Product getProductById(int productId) {
        return productRepositoryObject.findById(productId).orElseThrow(() -> new ResourceNotFoundException("Product not found with id " + productId));
    }

    @Override
    public Product createProduct(Product productObject) {
        return productRepositoryObject.save(productObject);

    }
    @Override
    public Product updateProduct(int productId, Product productObject) {
        Product fetchedProductObject=productRepositoryObject.findById(productId).orElseThrow();
        fetchedProductObject.setProductName(productObject.getProductName());
        fetchedProductObject.setProductPrice(productObject.getProductPrice());
        fetchedProductObject.setInventories(productObject.getInventories());
        return productRepositoryObject.save(fetchedProductObject);


    }
    @Override
    public String deleteProduct(int productId) {
        Product product = productRepositoryObject.findById(productId).orElseThrow(() -> new ResourceNotFoundException("Product not found with id " + productId));
        productRepositoryObject.delete(product);
        return "ProductId- "+productId+"Deleted";
    }
}
