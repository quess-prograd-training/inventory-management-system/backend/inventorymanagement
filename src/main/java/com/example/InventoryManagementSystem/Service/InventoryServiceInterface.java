package com.example.InventoryManagementSystem.Service;

import com.example.InventoryManagementSystem.Entity.Inventory;
import com.example.InventoryManagementSystem.Entity.Product;

public interface InventoryServiceInterface {


    Boolean isStockAvailable(Integer quantity, int productId);

    String getAllProductStockById(int productId);

    Integer getStockInProductById(int productId);

    String getAllProductStock();

    Product addStockByProductId(int productId, Inventory inventoryObject);
}
