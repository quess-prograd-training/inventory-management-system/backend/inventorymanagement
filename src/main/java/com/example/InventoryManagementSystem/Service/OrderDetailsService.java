package com.example.InventoryManagementSystem.Service;

import com.example.InventoryManagementSystem.Entity.OrderDetails;
import com.example.InventoryManagementSystem.Entity.User;
import com.example.InventoryManagementSystem.Repository.OrderDetailsRepository;
import com.example.InventoryManagementSystem.Repository.ProductRepository;
import com.example.InventoryManagementSystem.Repository.UserRepository;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class OrderDetailsService implements OrderDetailsServiceInterface {
    @Autowired
    ProductService productService;

    @Autowired
    UserRepository userRepository;
    @Autowired
    InventoryService inventoryServiceObject;
    @Autowired
    OrderDetailsRepository orderDetailsRepositoryObject;
    @Autowired
    private ProductRepository productRepository;


    @Override
    public OrderDetails getOrdersById(int id) {
        return orderDetailsRepositoryObject.findById(id).orElseThrow(() -> new ResourceNotFoundException("Order not found with id " + id));
    }

    @Override
    public String addOrders(OrderDetails orderDetailsObject) {
                if (inventoryServiceObject.isStockAvailable(orderDetailsObject.getProductQuantity(), orderDetailsObject.getProductId())) {
                    orderDetailsRepositoryObject.save(orderDetailsObject);
                    return "Order added to customer: " + orderDetailsObject.getUserKey().getUserId() + "\n" + "Product id: " + orderDetailsObject.getProductId() + "\nProduct Quantity: " + orderDetailsObject.getProductQuantity();
                } else {
                    return "sorry out of stock";
                }
            /*if (inventoryServiceObject.isStockAvailable(productQuantity, productId)) {
                Optional<Product> productOptional = productRepository.findById(productId);
                if (productOptional.isPresent()) {
                    Product product = productOptional.get();
                    orderDetailsObject.setProductId(productId);
                    orderDetailsObject.setProductQuantity(productQuantity);
                    orderDetailsObject.setProductPrice(product.getProductPrice() * productQuantity);
                    orderDetailsRepositoryObject.save(orderDetailsObject);
                    return orderDetailsObject.toString();
                }
                else {
                    throw new NoSuchElementException("No product found with id " + productId);
                }
            }
            else {
                throw new IllegalArgumentException("Stock not available for product with id " + productId);
            }*/
        }


        @Override
        public OrderDetails updateOrderDetails(int id, OrderDetails orderDetailsObject) {
            OrderDetails fetchedOrderDetailsObject= orderDetailsRepositoryObject.findById(id).get();
            if(fetchedOrderDetailsObject!=null)
            {
                orderDetailsRepositoryObject.delete(fetchedOrderDetailsObject);
                orderDetailsRepositoryObject.save(orderDetailsObject);
            }
            return fetchedOrderDetailsObject;
        }

        @Override
        public String deleteOrderDetails(int id) {
            OrderDetails orderDetails = orderDetailsRepositoryObject.findById(id).orElseThrow(() -> new ResourceNotFoundException("Order not found with id " + id));
            User user = orderDetails.getUserKey();
            user.getOrderDetailsKey().remove(orderDetails);
            orderDetailsRepositoryObject.delete(orderDetails);
            return "Order id: "+id+"deleted..";
        }

        @Override
        public String generateBillOfACustomerById(int userId) {
            User user=userRepository.findById(userId).get();
               Set<OrderDetails> orders=user.getOrderDetailsKey();
               StringBuilder str=new StringBuilder();
               str.append("Bill of UserID: "+userId+"\n");
               double subTotal=0;
            for (OrderDetails order:orders) {
               subTotal=subTotal+(order.getProductQuantity()*productService.getProductById(order.getProductId()).getProductPrice());
               str.append("OrderId: "+order.getId()+" ProductId: "+order.getProductId()+" ProductQty: "+order.getProductQuantity()+
                    " Price: "+productService.getProductById(order.getProductId()).getProductPrice()+" Amount: "+(order.getProductQuantity()*productService.getProductById(order.getProductId()).getProductPrice())+" \n");
            }
            str.append("\n*************************************************************************\nSubTotal: "+subTotal);

            return str.toString();
        }

}
