package com.example.InventoryManagementSystem.Service;

import com.example.InventoryManagementSystem.Entity.OrderDetails;

public interface OrderDetailsServiceInterface {



    OrderDetails getOrdersById(int id);

    String addOrders(OrderDetails orderDetailsObject);
    OrderDetails updateOrderDetails(int id, OrderDetails orderDetailsObject);

    String deleteOrderDetails(int id);

    String generateBillOfACustomerById(int userId);
}
