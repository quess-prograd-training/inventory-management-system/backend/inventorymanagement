package com.example.InventoryManagementSystem.Controller;

import com.example.InventoryManagementSystem.Entity.OrderDetails;
import com.example.InventoryManagementSystem.Service.OrderDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/orderDetails")
public class OrderDetailsController {

        @Autowired
        OrderDetailsService orderDetailsServiceObject;


        @PreAuthorize("hasAuthority('ADMIN')")
        @GetMapping("/get/{id}")
        public OrderDetails getOrderDetailsById(@PathVariable int id){
            return orderDetailsServiceObject.getOrdersById(id);
        }


        @PostMapping("/addOrders")
        public String addOrders(@RequestBody OrderDetails orderDetailsObject){
            return orderDetailsServiceObject.addOrders(orderDetailsObject);
        }


        @PreAuthorize("hasAuthority('ADMIN'),('MANAGER')")
        @PutMapping("/{id}")
        public OrderDetails updateOrderDetails(@PathVariable int id,@RequestBody OrderDetails orderDetailsObject){
            return orderDetailsServiceObject.updateOrderDetails(id,orderDetailsObject);
        }

        @PreAuthorize("hasAuthority('MANAGER')")
        @DeleteMapping("/{id}")
        public void deleteOrderDetails(@PathVariable int id) {
            orderDetailsServiceObject.deleteOrderDetails(id);
        }
       @GetMapping("generateBill/{userId}")
       public String generateBillOfACustomerById(@PathVariable("userId")int userId){
          return orderDetailsServiceObject.generateBillOfACustomerById(userId);
      }


}
