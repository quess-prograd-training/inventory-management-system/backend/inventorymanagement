package com.example.InventoryManagementSystem.Controller;

import com.example.InventoryManagementSystem.Entity.User;
import com.example.InventoryManagementSystem.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/users")
public class UserController {
    @Autowired
    UserService userServiceObject;

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/listOfUsers")
    public List<User> getAllUsers(){
        return userServiceObject.getAllUsers();
    }
    @PreAuthorize("hasAuthority('ADMIN','MANAGER')")
    @GetMapping("/{userId}")
    public User getUserById(@PathVariable int userId){
        return userServiceObject.getUserById(userId);
    }


    @PostMapping("/addUser")
    public User addUser(@RequestBody User userObject){
        return userServiceObject.addUser(userObject);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @PutMapping("/{userId}")
    public User updateUser(@PathVariable int userId, @RequestBody User userObject){
        return userServiceObject.updateUserById(userObject,userId);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/{userId}")
    public void deleteUser(@PathVariable int userId) {
        userServiceObject.deleteUserById(userId);
    }

}
