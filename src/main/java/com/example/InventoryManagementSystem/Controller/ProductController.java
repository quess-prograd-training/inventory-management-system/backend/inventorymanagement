package com.example.InventoryManagementSystem.Controller;

import com.example.InventoryManagementSystem.Entity.Inventory;
import com.example.InventoryManagementSystem.Entity.Product;
import com.example.InventoryManagementSystem.Repository.InventoryRepository;
import com.example.InventoryManagementSystem.Repository.ProductRepository;
import com.example.InventoryManagementSystem.Service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/products")
public class ProductController {
    @Autowired
    ProductService productServiceObject;
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private ProductRepository productRepository;

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/allProducts")
    public List<Product> getAllProducts(){
        return productServiceObject.getAllProducts();
    }


    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/get/{productId}")
    public Product getProductById(@PathVariable int productId){
        return productServiceObject.getProductById(productId);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/addProduct")
    public Product createProduct(@RequestBody Product productObject){
        Inventory inventory = new Inventory();
        inventory.setStockIn(productObject.getProductQuantity());
        inventoryRepository.save(inventory);
        productObject.setInventories(inventory);
        return productRepository.save(productObject);
    }


    @PreAuthorize("hasAuthority('ADMIN'),('MANAGER')")
    @PutMapping("/update/{productId}")
    public Product updateProduct(@PathVariable int productId,@RequestBody Product productObject){
        return productServiceObject.updateProduct(productId,productObject);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/delete/{productId}")
    public void deleteProduct(@PathVariable int productId) {
        productServiceObject.deleteProduct(productId);
    }

}
