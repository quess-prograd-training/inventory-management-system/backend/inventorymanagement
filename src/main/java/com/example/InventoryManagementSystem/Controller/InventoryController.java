package com.example.InventoryManagementSystem.Controller;

import com.example.InventoryManagementSystem.Entity.Inventory;
import com.example.InventoryManagementSystem.Entity.Product;
import com.example.InventoryManagementSystem.Service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("api/inventories")
public class InventoryController {
        @Autowired
        InventoryService inventoryServiceObject;

        @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('MANAGER')")
        @PutMapping("/{productId}")
        public String addStockByProductId(@RequestBody Inventory inventoryObject,@PathVariable("productId") int productId){
            Product product=inventoryServiceObject.addStockByProductId(productId,inventoryObject);
            return  "Stock added to product: "+product.getProductName()+"Total stock= "+product.getInventories().getStockIn();

        }

        @GetMapping("/getAllProductStock")
        public String getAllProductStock(){
            return inventoryServiceObject.getAllProductStock();
       }
       @GetMapping("/getStockByProductId/{productId}")
       public String getStockInByProductId(@PathVariable("productId") int productId){
            return inventoryServiceObject.getAllProductStockById(productId);
      }



}
