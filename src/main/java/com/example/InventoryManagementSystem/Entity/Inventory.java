package com.example.InventoryManagementSystem.Entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="Inventories")
public class Inventory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "inventoryId")
    private int inventoryId;

    @Column(name = "stockOut")
    private Integer stockOut;

    @Column(name = "stockIn", nullable = false)
    private Integer stockIn;

    @CreationTimestamp
    @Column(name = "createdAt")
    private LocalDateTime createdAt;

    @CreationTimestamp
    @Column(name = "updatedAt")
    private  LocalDateTime updatedAt;

    @OneToOne(mappedBy = "inventories",fetch = FetchType.EAGER)
    private Product productKey;

}
