package com.example.InventoryManagementSystem.Entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="orderDetails")
public class OrderDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name= "productId",nullable = false)
    @NotNull(message = "Product id cannot be null")
    private int productId;



    @Column(name = "quantity", nullable = false)
    private Integer productQuantity;

    @CreationTimestamp
    @Column(name = "createdAt")
    private LocalDateTime createdAt;

    @CreationTimestamp
    @Column(name = "updatedAt")
    private  LocalDateTime updatedAt;

    @Column(name = "productPrice")
    private double productPrice;
    @ManyToOne
    @JoinColumn(name = "userKey")
    private User userKey;




}
