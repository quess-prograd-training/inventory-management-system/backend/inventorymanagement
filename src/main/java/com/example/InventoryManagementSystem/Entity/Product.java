package com.example.InventoryManagementSystem.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "productId" )
    private int productId;

    @Size(max=50)
    @Column(name = "productName", unique = true)
    @NotNull(message = "Product name cannot be null ")
    private String productName;

    @Column(name = "productPrice")
    private double productPrice;

    @Column(name = "productQuantity", nullable = false)
    private Integer productQuantity;

    @CreationTimestamp
    @Column(name = "createdAt")
    private LocalDateTime createdAt;

    @CreationTimestamp
    @Column(name = "updatedAt")
    private LocalDateTime updatedAt;

    @OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "inventoryKey")
    @JsonIgnore
    private Inventory inventories;


}
